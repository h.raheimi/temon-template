const mongoose = require("mongoose");

const articleSchema = new mongoose.Schema({
  title: String,
  body: String,
  userID: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
});

module.exports = mongoose.model("article", articleSchema);