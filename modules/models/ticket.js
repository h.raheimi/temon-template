const mongoose = require("mongoose");

const ticketSchema = new mongoose.Schema(
  {
    title: { type: String, required: true },
    status: { type: Boolean },
    user: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
    description: [
      { img: { type: String } ,
       dec: { type: String, required: true } ,
       admin: { type: mongoose.Schema.Types.ObjectId, ref: "admin" } ,
        date: {type: Date, default: new Date()},
    },
    ],
  },
  { timestamps: true }
);
module.exports = mongoose.model("ticket", ticketSchema);
