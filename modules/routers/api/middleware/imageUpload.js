const multer = require("multer");
const uuid = require("uuid").v4;
const mkdirp = require("mkdirp");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const d = new Date();
    const dir = `./public/uploads/${d.getFullYear()}/${d.getMonth()}/${d.getDate()}`;
    // mkdirp(dir,err => cb(null, dir));

    // cb(null, dir);
    mkdirp(dir).then((made) => cb(null, dir));
  },
  filename: (req, file, cb) => {
    cb(
      null,
      `${uuid()}_${file.originalname.replace(/\@/g, "").replace(/\%/g, "")}`
    );
  },
});
const fileFilter = (req, file, cb) => {
  if (file.mimetype == "image/jpeg" || file.mimetype == "image/png") {
    cb(null, true);
  }
  else{
    cb(null, false);
  }
};
const upload = multer({
  limits: { fileSize: 1024 * 1024 * 1 },
  storage: storage,
  fileFilter: fileFilter,
});

module.exports = { upload };
