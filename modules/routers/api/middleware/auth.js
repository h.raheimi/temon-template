const jwt = require("jsonwebtoken");
const user = require("../../../models/user");

const auth = async (req, res, next) => {
  try {
    var token = req.cookies.token;
    const token2= jwt.verify(token, "Hossein1377");
    const User = await user.findOne({ token2 },["-password"]);
    if (User) {
      next();
    } else {
      res.status(401).json({ message: "Token is invalid" });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: err , message: "دسترسی وجود ندارد"});
  }
};

module.exports = auth;
