const sanitize = require("mongo-sanitize");

const mongoSanitize = (req,res,next) =>{
    req.body = sanitize(req.body);
    next();
}

module.exports = mongoSanitize;