const { Router } = require("express");

const router = Router();

router.use("/", require("../api/api-v1"));
router.use("/admin", require("../api/admin"));

module.exports = router;
