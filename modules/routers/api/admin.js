const { Router } = require("express");
const adminController = require("../../controller/api/v1/adminController");
const auth = require('./middleware/auth');

const router = Router();


router.get("/",auth ,adminController.Home.bind(adminController));

router.post("/add/admin", adminController.addAdmin.bind(adminController));

module.exports = router;