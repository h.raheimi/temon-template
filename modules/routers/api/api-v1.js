const { Router } = require("express");
const { upload } = require("./middleware/imageUpload");

const apiController = require("../../controller/api/v1/userController");
const UploadController = require("../../controller/api/v1/uploadController");
const ticketController = require("../../controller/api/v1/ticketController");
const userController = require("../../controller/api/v1/userController");

const router = Router();

router.get("/", apiController.show.bind(apiController));

router.get("/Response/:id", ticketController.Response.bind(ticketController));

router.post("/Response", ticketController.postResponse.bind(ticketController));

router.get("/search", userController.searchUser.bind(userController));

router.post("/UpdateMany", userController.updateManyUsers.bind(userController));
router.post("/deleteMany", userController.deletedMany.bind(userController));
router.post("/addattr", userController.addattr.bind(userController));

router.post(
  "/upload/:id",
  upload.single("image"),
  UploadController.uploadImage.bind(UploadController)
);

router.post(
  "/addticket/:id",
  ticketController.addTicket.bind(ticketController)
);

router.post("/register", apiController.postRegister.bind(apiController));

router.post("/login", apiController.postLogin.bind(apiController));

router.put("/forgetPassword", apiController.forgetPassword.bind(apiController));

router.delete("/remove", apiController.remove.bind(apiController));

module.exports = router;
