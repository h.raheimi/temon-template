const { Router } = require("express");

const mainController = require("../../controller/api/web/mainController");

const router = Router();

router.get("/", mainController.Home.bind(mainController));



module.exports = router;
