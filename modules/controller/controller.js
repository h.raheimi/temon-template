const user = require("../models/user");
const ticket = require("../models/ticket");
const admin = require("../models/admin");
const attModel = require("../models/attr");
const nodemailer = require("nodemailer");
const fs = require("fs");
const handlebars = require("handlebars");
const ejs = require("ejs");

var transport = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "ho3ein000r@gmail.com",
    pass: "kqqcgmooubzjhvhr",
  },
  from: "ho3ein000r@gmail.com",
});

module.exports = class Controller {
  constructor() {
    this.model = { user, ticket, admin, attModel };
  }
  sendMail(user, image) {
    // const smtpTrransport = require("nodemailer-smtp-transport");
    var readFile = fs.readFileSync("./emailTemplates/index.html", "utf8");
    // var template = handlebars.compile(readFile);
    // var html = template({
    //   user,
    //   image
    // })
    var template = ejs.compile(readFile);
    var html = template({
      user,
      image
    })
    var mailOptions = {
      from: "Localhost <ho3ein000r@gmail.com>",
      to: "haselireza97@gmail.com",
      subject: "test",
      html: html,
    };

    transport.sendMail(mailOptions, (err, info) => {
      if (err) return console.log(err);
      return console.log("Email sent", +info.response);
    });
  }
  okStaus(res, Data = {}, status = 200) {
    res.status(status).json(Data);
  }
  serverErros(res, Data = {}, status = 503) {
    res.status(status).json(Data);
  }
  badRequest(res, Data = {}, status = 422) {
    res.status(status).json(Data);
  }
  notFound(res, Data = {}, status = 404) {
    res.status(status).json(Data);
  }
};
