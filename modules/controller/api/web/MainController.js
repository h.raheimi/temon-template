const Controller = require("../../controller");

module.exports = new (class adminController extends Controller {
  async Home(req, res) {
    try {
      var user = await this.model.user.find();
      user = JSON.parse(JSON.stringify(user));
      res.render("pages/index", {
        title: "Home",
        path: "/users",
        user,
      });
      // res.status(200).json(user);
    } catch (err) {
      console.log(err);
      return this.serverErros(res, {
        success: false,
      });
    }
  }

  async addAdmin(req, res) {}
})();
