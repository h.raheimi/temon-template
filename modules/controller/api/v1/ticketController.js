const Controller = require("../../controller");

module.exports = new (class Ticket extends Controller {
  async addTicket(req, res) {
    try {
      const { dec } = req.body;
      await this.model.ticket.create({
        ...req.body,
        user: req.params.id,
        description: [{ dec: dec }],
      });
      res.status(200).json({ success: true });
    } catch (e) {
      console.error(e);
      res.status(500).json({ error: "Error adding ticket" });
    }
  }
  async Response(req, res) {
    try {
      const ticket = await this.model.ticket.findById(req.params.id);
      res.status(200).json({ ticket: ticket });
    } catch (err) {
      console.error(err);
      res.status(500).json({ error: "Error Response ticket" });
    }
  }

  async postResponse(req, res) {
    try {
      const update = await this.model.ticket.findOneAndUpdate(
        req.query.ticket,
        {
         $push: {description: { dec: req.body.dec, admin: req.query.admin }},
        },
        {
          returnOriginal: false,
        }
      );
      res.status(200).json(update);
    } catch (err) {
      console.log(err);
      res.status(500).json({ erroe: "Error Response ticket" });
    }
  }
})();
