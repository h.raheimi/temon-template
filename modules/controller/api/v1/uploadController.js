const Controller = require("../../../../modules/controller/controller");

module.exports = new (class UploadController extends Controller {
  async uploadImage(req, res) {
    try {
          const id = req.params.id;
          if (req.file) {
            console.log(req.file);
            await this.model.user.findOneAndUpdate(id, {
              imgProfile: `/${req.file.path.replace(/\\/g, "/")}`,
            });
            console.log(`/${req.file.path.replace(/\\/g, "/")}`);
            res.status(200).send(`عکس با موفقیت آپلود شد`);
          } else {
            res.send("لطفا عکسی را انتخاب کنید");
          }
    } catch (err) {
      console.log(err)
    }
  }
})();
