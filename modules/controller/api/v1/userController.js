const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const Controller = require("../../controller");

module.exports = new (class apiController extends Controller {
  async show(req, res) {
    const User = await this.model.user.find({}, [
      "-password",
      "-codeID",
      "-addres",
      "-sheansID",
    ]);
    console.log(User);
    res.json(User);
  }

  async postRegister(req, res) {
    const { email } = req.body;
    const finde = await this.model.user.findOne({ email });
    if (!finde) {
      const User = await this.model.user.create(req.body);
      return res.send("done!");
    }
    console.log(req.body);
    return res.send("we have email like that");
  }

  async postLogin(req, res) {
    const { email } = req.body;
    let findeUser = await this.model.user.findOne({ email });
    findeUser = JSON.parse(JSON.stringify(findeUser));
    if (!findeUser) return res.send("not finde!");
    const password = bcrypt.compareSync(req.body.password, findeUser.password);
    if (password) {
      const token = jwt.sign({ user_id: findeUser._id }, "Hossein1377", {
        expiresIn: "1h",
      });
      res.cookie(token, { expiresIn: "1h" });

      this.sendMail(
        findeUser,
        "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1200px-Image_created_with_a_mobile_phone.png"
      );
      return res.send(token);
    }
    return res.send("password incurect");
  }
  async forgetPassword(req, res) {
    const { email, password } = req.body;

    const findeuser = await this.model.user.findOne({ email });
    if (!findeuser) return res.send("not finde!");
    findeuser.password = password;
    findeuser.save();
    res.send("password updated");

    // await this.model.user.findOneAndUpdate({email:email}, {password} ,{
    //     new: true
    // },(err,upadte) =>{
    //     if(err) return res.json({message: "not finde!"})
    //     return res.send(upadte)
    // });
  }

  async remove(req, res) {
    try {
      const { email } = req.body;
      const result = await this.model.user.findOneAndRemove(email);
      res.send("deleted!");
    } catch (err) {
      res.send(err);
    }
  }

  async searchUser(req, res) {
    const search = req.query.search;
    if (!search)
      return this.okStaus(res, { messge: "چیزی برای جستجو وجود ندارد" });
    // const SearchUsers = await this.model.user.find({
    //   email: new RegExp(search,'i'),
    // });
    const SearchUsers = await this.model.user
      .find({ email: { $regex: search, $options: "i" } })
      .limit(2);
    if (SearchUsers.length > 0) return this.okStaus(res, SearchUsers);
    return this.badRequest(res, { messge: "چیزی پیدا نشد" });
  }
  async updateManyUsers(req, res) {
    try {
      const users = await this.model.user.updateMany(
        { name: req.body.name },
        {
          name: req.body.user,
        }
      );
      res.send(users);
    } catch (err) {
      console.error(err);
    }
  }
  async deletedMany(req, res) {
    try {
      await this.model.user.deleteMany({ name: req.body.name });
      res.send("Done!");
    } catch (err) {
      res.send(err);
    }
  }
  async addattr(req, res) {
    try {
      const attr = req.body.input.split(",");
      console.log(attr);
      this.model.attModel.create({ title: attr });

      console.log("done !");
    } catch (err) {
      console.log(err);
    }
  }
})();
