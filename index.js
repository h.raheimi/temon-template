const express = require("express");
const cookieParser = require("cookie-parser");

const app = express();

const conectDB = require("./db");
const mongoSanitize = require("./modules/routers/api/middleware/mongo-sanitize");
const { engine } = require("express-handlebars");

conectDB();

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(express.static("/public"));
app.use(cookieParser());

app.use("/public", express.static("public/"));

app.use("/", require("./modules/routers/web/web"));
app.use("/api", mongoSanitize, require("./modules/routers/api/index"));
app.use((err, req, res, next) => {
  console.log(err);
  if (err.hasOwnProperty("code") && err.code == "LIMIT_FILE_SIZE") {
    if (err.field == "image") {
      console.log(err);
      res.status(200).json({
        message: "حجم تصویر شما زیاد است",
      });
    }
  } else {
    res.status(200).json({
      message: { ...err },
    });
  }
});

app.engine(
  "hbs",
  engine({
    defaultLayout: "main",
    extname: ".hbs",
    layoutsDir: `${__dirname}/views/pages`,
    partialDir: `${__dirname}/views/components`,
  })
);
// app.set("views", "/views");
app.set("view engine", "hbs");

app.listen(3000, () => {
  console.log("server is running on  http://localhost:3000/");
});
